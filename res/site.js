$(window).load(function(){
	
		var slides = $('#slideshow li'), curent = 0;
		
		$('#slideshow ul').css('left', function() {
			return slides.width()/2 - $('#slideshow .SlideActiv img').width()/2;
		});
		
		$('#slideshow .SlideActiv img').click(function(){
			window.open($('#slideshow .SlideActiv img').attr('src'),"Poza","status=0,toolbar=0,resizable=0");
		});
		
		$('#slideshow .sageata').click(function(){
			var li = slides.eq(curent), urmIndex = 0;
			if($(this).hasClass('next')){
				urmIndex = curent >= slides.length-1 ? 0 : curent+1;
			}
			else {
				urmIndex = curent <= 0 ? slides.length-1 : curent-1;
			}
			
			var urm = slides.eq(urmIndex);
			curent=urmIndex;
			urm.addClass('slideActiv').show();
			li.removeClass('slideActiv').hide();
			
			$('#slideshow .SlideActiv img').click(function(){
				window.open($('#slideshow .SlideActiv img').attr('src'),"Poza","status=0,toolbar=0,resizable=0");
			});
		
			$('#slideshow ul').css('left', function() {
				return slides.width()/2 - $('#slideshow .SlideActiv img').width()/2 ;
			});
		});
});